import {StyleSheet} from 'react-native';


export const utilStyle = StyleSheet.create({
    container : {
        flex : 1,
        marginTop : 50,
        padding : 10,
        backgroundColor : '#E1F5FE'
    }
});
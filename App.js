import React, { Component } from 'react';
import { View, Text,TextInput,StyleSheet,ScrollView,TouchableOpacity,Alert } from 'react-native';
import {Link} from 'react-router-native';
import {utilStyle} from './util/css';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      txt_title:''
    };
  }

  alertMessageOfText = ()=>{
    Alert.alert(this.state.txt_title);
  }

  getMoviesFromApiAsync = async () => {
    
  }

  render() {
    return (
      <View style={utilStyle.container}>
        <ScrollView>
            <Text style={styles.title}> {this.state.txt_title} </Text>
            <TextInput style={styles.txtInput} placeholder="Enter App Title ... " onChangeText={(txt) => {this.setState({txt_title:txt})}} ref={(input) => this.inputRef = input}></TextInput>
            <TouchableOpacity onPress={()=>{this.alertMessageOfText()}}>
                <Text style={[styles.txtInput,{width : '80%',textAlign:'center',backgroundColor:'#8C9EFF'}]}>Submit</Text>
            </TouchableOpacity>

            <Link to="/detail">
              <Text style={styles.title}>Details Sayfasına Geç</Text>
            </Link>

        </ScrollView>
      </View>
    );
  }


}

const styles = StyleSheet.create({
  title : {
    textAlign : "center",
    fontSize : 52
  },txtInput : {
      marginTop : 15,
      marginBottom : 5,
      borderWidth : 1,
      borderRadius: 5,
      fontSize:25,
      padding:10
  }
});
